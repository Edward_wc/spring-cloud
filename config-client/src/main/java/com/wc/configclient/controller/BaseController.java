package com.wc.configclient.controller;

/*   author:WangChen
 *   date:2018/7/16
 */

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class BaseController {

    @Value("${hello}")
    String hello;

    @RequestMapping("hello")
    public String Hello() {
        return hello;
    }

}
